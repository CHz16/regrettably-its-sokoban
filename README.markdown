Regrettably, It's Sokoban
=========================

A haunted cartridge for [GAMES MADE QUICK??? III-2](https://itch.io/jam/games-made-quick-iii-2). Playable/downloadable here: https://chz.itch.io/regrettably-its-sokoban

This is a [GB Studio](https://www.gbstudio.dev) project. All the code, new graphics, and sounds here are by me and released under MIT (see LICENSE.txt). This project also includes some default GB Studio graphical assets (both in `assets/ui` and my maps and sprites), released by Chris Maltby under [MIT](https://opensource.org/licenses/MIT).
